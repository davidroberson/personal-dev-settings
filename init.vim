"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Source other files
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" let initpath = "Users/david.roberson/Development/Repositories/Personal/personal-dev-settings"
" Users/david.roberson/Development/Repositories/Personal/personal-dev-settings
" source initpath . "/test.vim"
"  source Users/david.roberson/development/personal/personal-dev-settings/test.vim
"  source Users/david.roberson/development/personal/personal-dev-settings/plugins.vim

lua << EOF
vim.keymap.set('n', 'Y', 'Y')
vim.keymap.set('n', 'D', '"_dd')
vim.keymap.set('n', 'C', '"_cc')
EOF

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Miscellaneous
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" map <Space> <Leader>

" Testing
nnoremap <Leader><Space> :echom strftime("%c")<CR>
" nnoremap <C-l> :echom strftime("%c")<CR>

nnoremap ; :
nnoremap : ;

" Center screen on entering insert mode
" autocmd InsertEnter * norm zz

" Disable auto inserting comments
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Toggle comment (ctrl+/)
nnoremap <C-\/> gcc
xnoremap <C-\/> gc

" Split line noop
" nnoremap J Jh
" nnoremap K li<CR><esc>k$

" Insert new line from normal mode
" nnoremap <Enter> o<Esc>

" Delete line from normal mode
" nnoremap <Backspace> kdd
" nnoremap <Delete> dd

" Visual select to end of line, but not newline
vnoremap $ $h

" Move lines
" nnoremap J :m .+1<CR>==
" nnoremap K :m .-2<CR>==

" nnoremap J :call VSCodeNotify('editor.action.moveLinesDownAction')<CR>
" nnoremap K :call VSCodeNotify('editor.action.moveLinesUpAction')<CR>

" vnoremap J :m '>+1<CR>gv=gv
" vnoremap K :m '<-2<CR>gv=gv
" xnoremap J <Cmd>call MoveVisualDown()<CR>

" Indenting
vnoremap > >gv
vnoremap < <gv

" Macro convenience
nnoremap Q @q

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= VSCode specific things
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" convert visual mode selection to real selection
" vnoremap s :<C-u>call <SID>copyToClipboard()<CR> 

" function! s:copyToClipboard()
"     normal! gv
"     let visualmode = visualmode()
"     if visualmode == "V"
"         let startLine = line("v")
"         let endLine = line(".")
"         call VSCodeNotifyRange("editor.action.clipboardCopyAction", startLine, endLine, 1)
"         :execute "normal! \<esc>"
"     else
"         let startPos = getpos("v")
"         let endPos = getpos(".")
"         call VSCodeNotifyRangePos("editor.action.clipboardCopyAction", startPos[1], endPos[1], startPos[2], endPos[2], 1)
"         :execute "normal! \<esc>"
"     endif
" endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Formatting
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" nnoremap <Leader>= <Cmd>call VSCodeNotify('editor.action.formatDocument')<CR>
" nnoremap <Leader>; <Cmd>call VSCodeNotify('editor.action.formatDocument')<CR>
nnoremap <Leader>= <Cmd>call VSCodeNotify('editor.action.formatSelection')<CR>
xnoremap <Leader>= :<C-u>call <SID>VSCodeFormatRange()<CR>

function! s:VSCodeFormatRange()
    normal! gv
    let visualmode = visualmode()
    let startLine = line("v")
    let endLine = line(".")
    call VSCodeCallRange('editor.action.formatSelection', startLine, endLine, 1)
    :execute "normal! \<esc>"
endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Jump-related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" (Remap in the hopes that one day the keepjumps command will work)

" Jump by multiple lines
" nnoremap <C-j> jjjjj
" nnoremap <C-k> kkkkk
" xnoremap <C-j> jjjjj
" xnoremap <C-k> kkkkk

" 
" nnoremap H <Cmd>keepjumps<Cmd>normal gg<CR>
" nnoremap M :exe 'keepjumps normal M'
" nnoremap L :exe 'keepjumps normal L'

" Why doesn't this work?
" nnoremap <S-z> zz
" nnoremap <Leader>z zz

" This is mapped directly in vscode
" nnoremap <C-u> <PageUp>
" nnoremap <C-d> <PageDown>

" Jump to members (i.e. methods)
nnoremap ¢ <Cmd>call VSCodeNotify('gotoNextPreviousMember.nextMember')<CR>
nnoremap ∞ <Cmd>call VSCodeNotify('gotoNextPreviousMember.previousMember')<CR>
xnoremap ¢ <Cmd>call VSCodeNotify('gotoNextPreviousMember.nextMember')<CR>
xnoremap ∞ <Cmd>call VSCodeNotify('gotoNextPreviousMember.previousMember')<CR>

" Jump through changelist
" nnoremap <C-l> g; " Older
" nnoremap <C-k> g, " Newer

" VSCode - Show symbols ( show script methods and variables )
nnoremap <Leader>o <Cmd>call VSCodeNotify('workbench.action.gotoSymbol')<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Copy/paste
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

nnoremap Y Y

" nnoremap p :call PasteIntuitive('p')<CR>
" nnoremap P :call PasteIntuitive('P')<CR>
" xnoremap p "_d"0P
" xnoremap P "_d"0P
nnoremap p "0p
nnoremap P "0P
xnoremap p "0P
xnoremap P "0P

" Pastes from the delete register when using (space+p)
nnoremap <Leader>p :execute "normal! \"" . v:register . "p"<CR>
nnoremap <Leader>P :execute "normal! \"" . v:register . "P"<CR>
" xnoremap <Leader>p :execute "visual! \"" . v:register . "p"<CR>
" xnoremap <Leader>P :execute "visual! \"" . v:register . "P"<CR>

" Cut (shift+X)
nnoremap X yydd
xnoremap X "0d

" Copy/paste using system clipboard
" (in vim-system-copy/plugin/system_copy.vim)

" Pastes from the yank register, instead of the delete register
function PasteIntuitive(pasteKey)
  let registerName = v:register
  if (registerName == "\"")
    " echom 'paste from "0'
    " :execute "normal! \"0p=`]"
    " :execute "normal! \"0" . a:pasteKey . "=`]"
    :execute "normal! \"0" . a:pasteKey
  else
    " echom 'paste from "' . v:register
    " :execute "normal! \"" . registerName . "p=`]"
    " :execute "normal! \"" . registerName . a:pasteKey . "=`]"
    :execute "normal! \"" . registerName . a:pasteKey
  endif
endfunction
 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Search related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set ignorecase
set smartcase

nnoremap <Leader>/ :set hlsearch!<CR>
nnoremap n :set hlsearch<CR>n
nnoremap N :set hlsearch<CR>N

" Search for selected text, forwards or backwards.
" https://vim.fandom.com/wiki/Search_for_visually_selected_text

vnoremap <silent> * :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy/<C-R>=&ic?'\c':'\C'<CR><C-R><C-R>=substitute(
  \escape(@", '/\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gVzv:call setreg('"', old_reg, old_regtype)<CR>
vnoremap <silent> # :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy?<C-R>=&ic?'\c':'\C'<CR><C-R><C-R>=substitute(
  \escape(@", '?\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gVzv:call setreg('"', old_reg, old_regtype)<CR>

nnoremap * :let curwd='\<<C-R>=expand("<cword>")<CR>\>'<CR>:let @/=curwd<CR>:call histadd("search", curwd)<CR>:set hls<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Commenting
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" To comment lines
xmap gc  <Plug>VSCodeCommentary
nmap gc  <Plug>VSCodeCommentary
omap gc  <Plug>VSCodeCommentary
nmap gcc <Plug>VSCodeCommentaryLine

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Undo and redo
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Using built-in vim undo sets everything on fire
nnoremap u <Cmd>call VSCodeNotify('undo')<CR>
nnoremap U <Cmd>call VSCodeNotify('redo')<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Marks
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Goes to beginning of line before creating mark
" nnoremap m 0m

" Default to scroll to top of screen when jumping to a mark
"function s:reveal(direction, resetCursor, restoreVisual)
"    if a:restoreVisual
"        normal! gv
"    endif
"    call VSCodeExtensionNotify('reveal', a:direction, a:resetCursor)
"endfunction
"
"let i = char2nr('a')
"while i <= char2nr('z')
"  execute "nnoremap `" . nr2char(i) . " '" . nr2char(i) . ":<C-u>call <SID>reveal('top', 0, 0)<CR>"
"  execute "nnoremap '" . nr2char(i) . " '" . nr2char(i) . ":<C-u>call <SID>reveal('top', 0, 0)<CR>"
"let i += 1
"endwhile

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Load matchit plugin
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

packadd! matchit

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Jump to inner and outer blocks
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

nnoremap <Leader>h [{

" Searches for next occurence of block
" (Saves and restores search history)
nnoremap <Leader>l :set nohlsearch<CR>:let @a = @/<CR>/^\_s*{<CR>e:let @/ = @a<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Remap default vscode-neovim shortcuts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" nnoremap gh <Cmd>call VSCodeNotify('editor.action.showDefinitionPreviewHover')<CR>
" xnoremap gh <Cmd>call VSCodeNotify('editor.action.showDefinitionPreviewHover')<CR>

" nnoremap <Leader>d <Cmd>call <SID>vscodeGoToDefinition("Definition")<CR>
" xnoremap <Leader>d <Cmd>call <SID>vscodeGoToDefinition("Definition")<CR>
" function! s:vscodeGoToDefinition(str)
"     if exists('b:vscode_controlled') && b:vscode_controlled
"         exe "normal! m'"
"         call VSCodeNotify('editor.action.reveal' . a:str)
"     else
"         " Allow to funcionar in help files
"         exe "normal! \<C-]>"
"     endif
" endfunction

" nnoremap <Leader>D <Cmd>call VSCodeNotify('editor.action.peekDefinition')<CR>
" nnoremap <Leader>r <Cmd>call VSCodeNotify('editor.action.referenceSearch.trigger')<CR>
nnoremap gr <Cmd>call VSCodeNotify('editor.action.referenceSearch.trigger')<CR>

" xnoremap <Leader>D <Cmd>call VSCodeNotify('editor.action.peekDefinition')<CR>
" xnoremap <Leader>r <Cmd>call VSCodeNotify('editor.action.referenceSearch.trigger')<CR>
xnoremap gr <Cmd>call VSCodeNotify('editor.action.referenceSearch.trigger')<CR>

nnoremap <C-w>d <Cmd>call VSCodeNotify('editor.action.revealDefinitionAside')<CR>
xnoremap <C-w>d <Cmd>call VSCodeNotify('editor.action.revealDefinitionAside')<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Remap VSCode Quick Actions menu
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

function! OpenVSCodeQuickFix()
    let mode = mode()
    if mode ==# 'V'
      let startLine = line("v")
      let endLine = line(".")
      call VSCodeNotifyRange("editor.action.quickFix", startLine, endLine, 1)
    else
      let startLine = line("v")
      let endLine = line(".")
      call VSCodeNotifyRange("editor.action.quickFix", startLine, endLine, 1)
    endif
endfunction

nnoremap <silent> <C-z> <Cmd>call OpenVSCodeQuickFix()<CR>
xnoremap <silent> <C-z> <Cmd>call OpenVSCodeQuickFix()<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= To allow moving past folded code
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" (Seems to cause problems)

"nmap j gj
"nmap k gk
