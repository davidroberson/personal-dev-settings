"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Plugins will be downloaded under the specified directory.
call plug#begin('Users/david.roberson/Development/vim-plugins')

" Declare the list of plugins.
Plug 'asvetliakov/vim-easymotion'
Plug 'haya14busa/incsearch.vim'
Plug 'haya14busa/incsearch-easymotion.vim'
Plug 'tpope/vim-surround'
Plug 'machakann/vim-highlightedyank'
Plug 'christoomey/vim-system-copy'
Plug 'davidroberson3/vim-indent-object', { 'branch': 'exclude-blank-line-selection' }
Plug 'unblevable/quick-scope'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Yank highlight
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:highlightedyank_highlight_duration = 100


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Easymotion
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:EasyMotion_do_shade = 1
let g:EasyMotion_do_mapping = 0 " Disable default mappings
let g:EasyMotion_smartcase = 1
let g:EasyMotion_use_upper = 1
let g:EasyMotion_grouping = 2
let g:EasyMotion_keys = 'asdfghjkl;qwertyuiopzxcvbnm'
let g:EasyMotion_off_screen_search = 1
let g:EasyMotion_space_jump_first = 1
let g:EasyMotion_disable_two_key_combo = 1
let g:EasyMotion_use_smartsign_us = 1

map <leader>; <Plug>(easymotion-bd-w)
map <leader>: <Plug>(easymotion-bd-jk)

" map <Leader>s <Plug>(easymotion-s2)

" nmap <Leader>; <Plug>(easymotion-next)
" nmap <Leader>, <Plug>(easymotion-prev)

"map <Leader>:  <Plug>(easymotion-bd-jk)
"nmap <Leader>f <Plug>(easymotion-s)
"nmap <Leader>s <Plug>(easymotion-sn)
"xmap <Leader>s <Plug>(easymotion-sn)
"omap <Leader>z <Plug>(easymotion-sn)

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Incsearch + Easymotion
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" map z/ <Plug>(incsearch-easymotion-/)
" map z? <Plug>(incsearch-easymotion-?)
" map zg/ <Plug>(incsearch-easymotion-stay)

" incsearch.vim x fuzzy x vim-easymotion

" function! s:config_easyfuzzymotion(...) abort
"   return extend(copy({
"   \   'converters': [incsearch#config#fuzzy#converter()],
"   \   'modules': [incsearch#config#easymotion#module()],
"   \   'keymap': {"\<CR>": '<Over>(easymotion)'},
"   \   'is_expr': 0,
"   \   'is_stay': 1
"   \ }), get(a:, 1, {}))
" endfunction

" noremap <silent><expr> <Space>z/ incsearch#go(<SID>config_easyfuzzymotion())

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"= Quick scope
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" highlight QuickScopePrimary guifg='#afff5f' gui=underline ctermfg=155 cterm=underline
" highlight QuickScopeSecondary guifg='#5fffff' gui=underline ctermfg=81 cterm=underline
" highlight QuickScopePrimary guifg='#d000d0' gui=underline ctermfg=155 cterm=underline
" highlight QuickScopeSecondary guifg='#f0f000' gui=underline ctermfg=81 cterm=underline
highlight QuickScopePrimary guifg='#d000d0' ctermfg=155 
highlight QuickScopeSecondary guifg='#d000d0' guisp='#d000d0' gui=underline ctermfg=81 
" highlight QuickScopePrimary guifg='#d000d0' guisp='#5fffff' gui=underline ctermfg=155 cterm=underline
" highlight QuickScopeSecondary guifg='#f0f000' guisp='#5fffff' gui=underline ctermfg=81 cterm=underline

let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']
